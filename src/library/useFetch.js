import {useState, useEffect} from 'react'

//Hook personnalise permettant de rendres les items d'une api et un loader
export function useFetch(url)
{
	const [items, setItems] = useState([])
	const [loading, setLoading] = useState(true)

	useEffect(() => {
		(async function()
		{
			const response = await fetch(url);
			const responseData = await response.json();

			if(response.ok)
			{
				setItems(responseData)
				setLoading(false)
			}
			else
 			{
				alert(JSON.stringify(responseData))
				setLoading(false)
			}

		})()

	}, [loading])
	return [
		loading,
		items
	]
}
