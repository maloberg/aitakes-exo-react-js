import React from 'react'
import Slider from './components/Slider'
import Header from './components/Header'
import './Home.scss'
export default function Home() {
	//page d'accueil
	return (
		<>
			<Slider></Slider>
			<div className="title"> <h1>Bonjour je m'appel Bob</h1></div>
			<div className="container">
				<h2>BOB STORY</h2>
				<p>
					Voici l'histoire de Bob le grand mechant.
					Bob etait un grand mechant boulanger qui aimait le pain et puis un jour
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore nisi commodi, 
					minus laborum deleniti fugiat aliquid adipisci tempora praesentium pariatur 
					amet maiores impedit eos doloribus aut quidem accusantium expedita tempore!
				</p>
				<p>
					Et voila c'etait la fameuse histoire de Bob
					BERGADE 
				</p>
			</div>
		</>
	)
}
