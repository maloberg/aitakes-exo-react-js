import './App.scss';
import React from "react";
import Home from "./Home"
import HandleListe from "./components/HandleListe"
import Header from "./components/Header"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";


function App() {
  return (
    // Utilisation de react router pour gerer les urls
    <div>
    <Router>
      <Header/>
      <Switch>
        <Route path="/" exact component={Home}/>
        <Route path="/list" component={HandleListe}/>
      </Switch>
    </Router>
    </div>
  );
}

export default App;
