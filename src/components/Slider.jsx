import React from 'react'
import {Slide} from 'react-slideshow-image'
import 'react-slideshow-image/dist/styles.css'
import './Slide.scss'
import img1 from '../img/img1.jpeg';
import img2 from '../img/img2.jpeg';
import img3 from '../img/img3.jpeg';
const proprietes = {
	duration:5000,
	transitionDuration:500,
	infinite: true,
	indicators: true,
	arrows: false
}

//Utilisation d'un bundle pour mettre en place le slider
export default function Slider() {
	return (
		<>
		<div className="containerSlide">
			<Slide {...proprietes}>
				<div className="each-slide">
					<div>
						<img src={img1} alt="img" />
					</div>
				</div>
				<div className="each-slide">
					<div>
						<img src={img2} alt="img" />
					</div>
				</div>
				<div className="each-slide">
					<div>
						<img src={img3} alt="img" />
					</div>
				</div>
			</Slide>
		</div>
		</>
	)
}
