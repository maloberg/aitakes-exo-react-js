import React from 'react'
import {Link} from "react-router-dom";
export default function Navigation() {
	return (
		<nav>
			<ul>
				<Link to="/"><li>Bonjour</li></Link>
				<Link to="/list"><li>Listes</li></Link>
			</ul>
		</nav>
	)
}
