import React from 'react'
import './Header.scss'
import Navigation from './Navigation'
import {Link} from "react-router-dom";
export default function Header() {
	return (
		<header>
			<Link to="/"><div className="logo">HOME</div></Link>
			<Navigation/>
		</header>
	)
}
