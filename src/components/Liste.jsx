
import './Liste.scss'
import React, {useState} from 'react'
import {useFetch} from '../library/useFetch'
import Modal from './Modal'


export default function Liste({inputValue}) {
	const [modal, setModal] = useState(false)
	const [datatab, setDatatab] = useState({})

	//Operation reussie	// Tentative de Popup modal en passant des props
	const showModal = (title, description, imgurl) =>
	{
		setDatatab({  	
			"title" :title,
			"description" :description,
			"imgurl" :imgurl
		})
		setModal(true)
	}
	
	const closeModal = ()=>
	{
		setModal(false)
		setDatatab({})
	}

	// Pattern pour chaque "card"
	const ListeCard = ({title, description, imgurl}) =>
	{
		return(
			<div className="card" onClick={() =>  showModal(title, description, imgurl)}>
				<img src={imgurl} alt="bob" />
				<div>{title}</div>
			</div>
		)
	}

	
	// Utilisation d'un hook personalise pour interagir avec l'api
	const [loading, items] = 
		useFetch('https://api.nytimes.com/svc/books/v3/lists/current/hardcover-fiction.json?api-key=TA0YR2IIJVhseTJWYgGebidcA24WuNgH')	
	const books = [];

	// Verification du chargement des intructions de l'api
	if(!loading)
	{
		items.results.books.map(element =>
		{
			//Systeme de tri lie a la barre de recherche 
			if(element.title.indexOf(inputValue.toUpperCase()) == -1)
			{
				return
			}
			// Creation du tableau d'affichage
			books.push(<ListeCard key={element.rank} title={element.title} 
				description={element.description} imgurl={element.book_image}/>)							
		})
	}

	return (
		<div className="books">
			{loading && <div>..chargement</div>}
			{books}
			<Modal showModal={modal} closeModal={closeModal}>
				<div className="modalImg">
					<div>
						<img src={datatab.imgurl} alt="" />
					</div>
				</div>
				<div className="modalHeader">
					<h2>{datatab.title}</h2>
				</div>
				<div className="modalBody">
					<p>{datatab.description}</p>
				</div>
				<div className="modalFooter">
					<button className="modal-btn">fermer</button>
				</div>
			</Modal>
		</div>
	)
}