import React from 'react'
import './Modal.scss'

//Component PopUp
export default function Modal({showModal, children, closeModal}) {
	return (
		showModal &&(
			<div className="modalBackground" onClick={closeModal}>
				<div className="modalContainer">
					{children}
				</div>	
			</div>
		)
	)
}
