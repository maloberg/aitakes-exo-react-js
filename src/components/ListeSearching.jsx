import React from 'react'
//Entrainement pour passer les props entre plusieurs components
export default function ListeSearching({onSearchHandleChange, inputValue}) {

	const handleChange = (e) =>
	{
		onSearchHandleChange(e.target.value);
	}

	return (
		<div className="search">
			<input type="text" value={inputValue} onChange={handleChange} placeholder="Rechercher..."/>
		</div>
	)
}
