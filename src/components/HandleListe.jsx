import React, {useState} from 'react'
import Liste from './Liste'
import ListeSearching from './ListeSearching'
import Modal from './Modal'
import './HandleListe.scss';

// Entrainement pour passer des props de parents a enfants

export default function HandleListe() {
	const[input, setInput] = useState('')
	const onHandleChange = (value) =>
	{
		console.log(value);
		setInput(value)
	}

	return (
		<div className="liste-container">
			<ListeSearching inputValue={input} onSearchHandleChange={onHandleChange}/>
			<Liste inputValue={input} />
			
		</div>
	)
}
